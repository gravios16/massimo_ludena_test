<?php

class CompleteRange
{
    //$numbers: Array of integer numbers ordered by ascendent
    //return: Array of integer numbers ordered by ascendent
    public function build($numbers)
    {
        $response = [];
        $i        = 0;

        do {
            if ($i == 0) {
                array_push($response, $numbers[$i]);
            } else {
                while ($response[sizeof($response) - 1] < $numbers[$i]) {
                    array_push($response, ($response[sizeof($response) - 1] + 1));
                };
            }

            $i++;
        } while ($response[sizeof($response) - 1] < $numbers[sizeof($numbers) - 1]);

        return $response;
    }
}
