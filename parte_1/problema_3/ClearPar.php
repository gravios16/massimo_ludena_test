<?php

class ClearPar
{

    //$str: String
    //return: String
    public function build($str)
    {
        $array_str  = str_split($str);
        $new_array  = [];
        $last_close = true;

        for ($i = 0; $i < sizeof($array_str); $i++) {

            if ($array_str[$i] == ")" && sizeof($new_array) > 0) {
                if ($new_array[sizeof($new_array) - 1] == "(") {
                    array_push($new_array, $array_str[$i]);
                    $last_close = true;
                }
            }

            if ($array_str[$i] == "(" && $i < sizeof($array_str) - 1 && $array_str[$i + 1] == ")") {

                if (sizeof($new_array) == 0) {
                    array_push($new_array, $array_str[$i]);
                    $last_close = false;
                } else {
                    if ($new_array[sizeof($new_array) - 1] == ")" && $last_close == true) {
                        array_push($new_array, $array_str[$i]);
                        $last_close = false;
                    }
                }

            }

        }

        return implode("", $new_array);
    }

}
