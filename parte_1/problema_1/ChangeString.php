<?php

class ChangeString
{
    protected $alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

    //Constructor
    //$alphabet: Array
    public function __construct($alphabet = null)
    {
        if ($alphabet !== null) {
            $this->alphabet = $alphabet;
        }
    }

    //$str: String
    //return: String
    public function build($str)
    {
        $array_str = str_split($str);

        for ($i = 0; $i < sizeof($array_str); $i++) {

            for ($j = 0; $j < sizeof($this->alphabet); $j++) {

                if (strtolower($array_str[$i]) == $this->alphabet[$j]) {

                    $alphabet_pos  = ($j < sizeof($this->alphabet) - 1) ? $j + 1 : 0;
                    $letter        = $this->alphabet[$alphabet_pos];
                    $array_str[$i] = ctype_upper($array_str[$i]) ? strtoupper($letter) : $letter;
                    break;

                }

            }

        }

        return implode("", $array_str);
    }

}
