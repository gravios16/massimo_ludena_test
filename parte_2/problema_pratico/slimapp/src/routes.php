<?php
// Routes

//WEB
$app->get('/', function ($request, $response, $args) {
    $employees = json_decode(file_get_contents("../public/employees.json"), true);
    $data      = [];
    foreach ($employees as $employee) {
        $data[] = [
            'id'       => $employee['id'],
            'name'     => $employee['name'],
            'email'    => $employee['email'],
            'position' => $employee['position'],
            'salary'   => $employee['salary'],
        ];
    }
    return $this->renderer->render($response, "/employees/index.php", compact('data'));
});

$app->post('/search/employees', function ($request, $response, $args) {
    $employees = json_decode(file_get_contents("../public/employees.json"), true);
    $email     = $request->getParsedBody()['email'];
    $data      = [];
    if (isset($email)) {
        foreach ($employees as $employee) {
            if ($employee['email'] == $email) {
                $data[] = [
                    'id'       => $employee['id'],
                    'name'     => $employee['name'],
                    'email'    => $employee['email'],
                    'position' => $employee['position'],
                    'salary'   => $employee['salary'],
                ];
            }
        }
    }
    return $this->renderer->render($response, "/employees/index.php", compact('data', 'email'));
});
$app->get('/employees/{id}', function ($request, $response, $args) {
    $employees = json_decode(file_get_contents("../public/employees.json"), true);
    $id        = $args['id'];
    $data      = [];
    if (isset($id)) {
        foreach ($employees as $key => $employee) {
            if ($employee['id'] == $id) {
                $data = [
                    'Name'     => $employee['name'],
                    'E-mail'   => $employee['email'],
                    'Phone'    => $employee['phone'],
                    'Address'  => $employee['address'],
                    'Position' => $employee['position'],
                    'Salary'   => $employee['salary'],
                    'Skills'   => $employee['skills'],
                ];
            }
        }
    }
    return $this->renderer->render($response, "/employees/show.php", compact('data'));
});

//API
$app->get('/api/search/employees', function ($request, $response, $args) {
    $min       = $request->getQueryParam('min_salary', $default = null);
    $max       = $request->getQueryParam('max_salary', $default = null);
    $employees = json_decode(file_get_contents("../public/employees.json"), true);
    $xml       = "<root>";
    if ($min !== null || $max !== null) {
        foreach ($employees as $key => $employee) {
            $salary = floatval(str_replace(['$', ','], "", $employee['salary']));
            if ($salary <= $max && $salary >= $min) {
                $xml .= "<employee>";
                foreach ($employee as $key => $value) {
                    if ($key == "skills") {
                        $xml .= "<skills>";
                        foreach ($value as $skill) {
                            $xml .= "<skill>" . $skill['skill'] . "</skill>";
                        }
                        $xml .= "</skills>";
                    } else {
                        if ($value == false) {
                            $xml .= "<$key>0</$key>";
                        } else {
                            $xml .= "<$key>$value</$key>";
                        }
                    }

                }

                $xml .= "</employee>";
            }
        }
    }
    $xml .= "</root>";
    $body = $response->getBody();
    $body->write($xml);

    return $response->withHeader(
        'Content-Type',
        'application/xml'
    )->withBody($body);
});
