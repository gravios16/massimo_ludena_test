<!DOCTYPE html>
<html>
<head>
    <title>Employees</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

    <div class="col-md-6 col-md-offset-3" style="margin-top:30px;">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="/">Employees</a></h3>
                </div>
            <div class="panel-body">
                <form class="form-inline" action="/search/employees" method="POST">
                  <div class="form-group">
                    <label for="exampleInputEmail2">Search</label>
                    <input name="email" type="email" class="form-control" id="email" placeholder="E-mail" value="<?php echo isset($email) ? $email : ''; ?>">
                  </div>
                  <button type="submit" class="btn btn-info">Submit</button>
                </form>
                <table class="table table-striped">
                    <thead>
                        <th>
                            N°
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Position
                        </th>
                        <th>
                            Salary
                        </th>
                    </thead>

<?php foreach ($data as $key => $employee) {
    echo '<tr>';
    echo '<td>' . ($key + 1) . '</td>';
    foreach ($employee as $key => $value) {
        if ($key != 'id') {
            echo '<td>' . $value . '</td>';
        }

    }
    echo "<td><a href='/employees/" . $employee['id'] . "'>Detail</a></td>";
    echo '</tr>';
}?>

                </table>
            </div>
    </div>
</body>
</html>