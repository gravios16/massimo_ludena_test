<!DOCTYPE html>
<html>
<head>
    <title>Employees</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

    <div class="col-md-6 col-md-offset-3" style="margin-top:30px;">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="/">Employees</a></h3>
                </div>
            <div class="panel-body">
                <h3 style="margin-top:0px;"><a href="/">Employees</a> / Detail</h3>
            <ul class="list-group">
<?php foreach ($data as $key => $value) {
    if (is_array($value)) {
        echo '<li class="list-group-item ">' . $key . ' : ';
        foreach ($value as $skill) {
            if ($skill === end($value)) {
                echo $skill['skill'];
            } else {
                echo $skill['skill'] . ', ';
            }
        }
        echo '</li>';
    } else {
        echo '<li class="list-group-item ">' . $key . ' : ' . $value . '</li>';
    }
}?>
            </ul>
            </div>
    </div>
</body>
</html>