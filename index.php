<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

<div class="col-md-6 col-md-offset-3" style="margin-top:30px;">
    <div class="panel panel-default">
        <div class="panel-body text-center">
            Massimo German Ludeña Lezama
        </div>
    </div>
    <div class="alert alert-info text-center"  role="alert">
        <h3 style="margin-top:0px;">Instrucciones</h3>
      <ul style="list-style: none;">
          <li>
              <strong>Problema 1:</strong> Ingresar cualquier cadena de texto. Ejm: 123 bcde*​ 3
          </li>
          <li>
              <strong>Problema 2:</strong> Ingresar números enteros ordenados de forma ascendente separados por comas. Ejm: 1,2,4,5
          </li>
          <li>
              <strong>Problema 3:</strong> Ingresar una cadena de texto conformada por parentesis. Ejm: ()())()
          </li>
      </ul>
    </div>
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Part 01</h3>
      </div>
      <div class="panel-body">
        <form method="POST" action="index.php">
            <div class="form-group">
                <label for="input">Input</label>
                <input name="input" type="text" class="form-control" id="id_input" value='<?php echo isset($_POST['input']) ? $_POST['input'] : ''; ?>'>
            </div>
            <select name="problem" class="form-control">

<?php if (isset($_POST['problem'])) {?>
                <option value="1" <?php echo $_POST['problem'] == 1 ? 'selected' : ''; ?>>Problem 1 (ChangeString)</option>
                <option value="2" <?php echo $_POST['problem'] == 2 ? 'selected' : ''; ?>>Problem 2 (CompleteRange)</option>
                <option value="3" <?php echo $_POST['problem'] == 3 ? 'selected' : ''; ?>>Problem 3 (ClearPar)</option>
<?php } else {?>
                <option value="1">Problem 1 (ChangeString)</option>
                <option value="2">Problem 2 (CompleteRange)</option>
                <option value="3">Problem 3 (ClearPar)</option>
<?php }?>
            </select>
            <br>
            <div class="form-group">
                <label for="input">Output</label>
                <textarea class="form-control succes" rows="10">
<?php

include "parte_1/problema_1/ChangeString.php";
include "parte_1/problema_2/CompleteRange.php";
include "parte_1/problema_3/ClearPar.php";

switch (isset($_POST['problem']) ? $_POST['problem'] : -1) {
    case '1':
        $ChangeString = new ChangeString();
        echo $ChangeString->build($_POST['input']);
        break;
    case '2':
        $CompleteRange = new CompleteRange();
        print_r($CompleteRange->build(explode(',', $_POST['input'])));
        break;
    case '3':
        $ClearPar = new ClearPar();
        echo $ClearPar->build($_POST['input']);
        break;
    default:
        break;
}

?></textarea>
            </div>
            <br>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Part 02</h3>
            </div>
        <div class="panel-body">
            <p>Levantar el proyeto que se encuentra en la ruta:
            /massimo_ludena_test/parte_2/problema_practico/slimapp</p>

            <p>Copiar la carpeta "slimapp" y colocarla en htdocs de Xampp o en la carpeta de despliegue del servidor que use, en mi caso he utilizado el servidor virtual Homestead.</p>

            <p><strong>Importante:</strong><br>
                Las rutas han sido programadas de acuerdo a una dirección absoluta Ejm: slim.app,
                si utiliza un servidor local como xampp puede ocurrir errores en la rutas,
                en todo caso sera solucionado añadiendo "/slimapp/public" despues de "localhost".
            </p>

            <p>Ruta ejemplo para el consumo del servicio web XML:<br><strong>
            http://slim.app/api/search/employees?min_salary=3000&max_salary=5000</strong></p>
        </div>
    </div>
</div>


</body>
</html>
